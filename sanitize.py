from FileIO import FileIO
from SearchFilesInTheDirectory import SearchFilesInTheDirectory
from lxml import objectify

from SystemUtils import SystemUtils
from Utils import LxmlHelper
from argparse import ArgumentParser


class MainClass:
    __default_input_path_str: str = r"C:\Users\pc\source\repos\BitBucket\BioShell_unsorted"
    __default_output_path_str: str = r"C:\Users\pc\source\repos\BitBucket\BioShell___xml"

    @classmethod
    def __parse_args(cls):
        # initialize argument parser
        my_parser = ArgumentParser()
        my_parser.add_argument("-i", help="input directory", type=str)
        my_parser.add_argument("-o", help="output directory", type=str)

        # parse the argument
        args = my_parser.parse_args()

        if args.i is not None:
            cls.__default_input_path_str = args.i
        if args.o is not None:
            cls.__default_output_path_str = args.o

    @classmethod
    def main(cls):
        # parse command-line arguments
        cls.__parse_args()

        path_str = cls.__default_input_path_str
        SearchFilesInTheDirectory.do_indexing_of_files(path_str, "xml", is_messages=True)
        xml_file_path_list = SearchFilesInTheDirectory.get_all_file_paths_list()

        for xml_file_path in xml_file_path_list:
            print("Objectifying : [" + xml_file_path + "]")
            doxygen_obj = None
            try:
                doxygen_obj = LxmlHelper.objectify_xml(xml_file_path)
            except Exception as ex:
                print(str(ex))
                
            print("Moving : [" + xml_file_path + "]")
            if hasattr(doxygen_obj, "compounddef"):
                if hasattr(doxygen_obj.compounddef, "location"):
                    new_file_path = None
                    try:
                        file_location_obj = doxygen_obj.compounddef.location
                        cpp_file_path_str = file_location_obj.get("file")
                        file_name_str = SystemUtils.get_file_name(xml_file_path)
                        file_dir_str = SystemUtils.get_dir(cpp_file_path_str)
                        new_dir_str = SystemUtils.merge_paths(cls.__default_output_path_str, file_dir_str)
                        SystemUtils.create_dir(new_dir_str)
                        new_file_path = SystemUtils.merge_paths(new_dir_str, file_name_str)
                        SystemUtils.move(xml_file_path, new_file_path)
                    except Exception as e:
                        ##print(str(e))
                        print("From " + xml_file_path + "] to [" + new_file_path + "]")
                    ## END of try


if __name__ == '__main__':
    MainClass.main()
